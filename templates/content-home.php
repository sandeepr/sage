<section class="sage-section sage-section-hero sage-section-full sage-parallax-background sage-section-with-arrow sage-after-navbar" id="header1-1" style="background-image: url(https://mobirise.com/assets3/images/jumbotron-2000x1252-54.jpg);">

    <div class="sage-overlay" style="opacity: 0.6; background-color: rgb(0, 0, 0);"></div>

    <div class="sage-table-cell">

        <div class="container">
            <div class="row">
                <div class="sage-section col-md-10 push-md-1 text-xs-center">

                    <h1 class="sage-section-title display-1">WordPress Auction Software Solution</h1>
                    <p class="sage-section-lead lead">Turn any WordPress site into a professional auction website with WP Auction Software plugin<br></p>
                    <div class="sage-section-btn">
			<a class="btn btn-lg btn-primary" href="#">
				<span class="socicon socicon-windows sage-iconfont sage-iconfont-btn"></span>
				Buy Now
			</a>
			
		    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sage-arrow sage-arrow-floating" aria-hidden="true"><a href="#features6-3"><i class="sage-arrow-icon"></i></a></div>

</section>
<?php the_content(); ?>
<?php //wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
