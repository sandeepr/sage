<header id="ext_menu-0" class="banner">

    <nav class="navbar navbar-dropdown bg-color transparent navbar-fixed-top">
      <div class="container">
    <div class="sage-table">
                <div class="sage-table-cell">

                    <div class="navbar-brand">
                        <a href="#" class="navbar-logo"><img src="https://numixproject.org/res/img/numix-logo.png" alt="Mobirise"></a>
                        <a class="navbar-caption" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
                    </div>

                </div>
                <div class="sage-table-cell">

                    <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target="#nav-content">
                        <div class="hamburger-icon"></div>
                    </button>
  
    <?php
	      if (has_nav_menu('primary')) :
	      wp_nav_menu( array(
	      		'menu'              => 'primary',
	      		'theme_location'    => 'primary',
			'depth'		    => '1',
			'container' 	    => '',
			'container_class'   => 'menu-header',		
			'menu_id'     	    => 'nav-content',
	      		'menu_class'        => 'nav-dropdown collapse pull-xs-right nav navbar-nav navbar-toggleable-sm',
	      		'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	      		'walker'            => new wp_bootstrap_navwalker())
	      		);
	      endif;
	      ?>  	
		<button hidden="" class="navbar-toggler navbar-close" type="button" data-toggle="collapse" data-target="#nav-content">
                        <div class="close-icon"></div>
                </button>
	</div>
      </div>
  </div>	
</nav>
</header>


<!--<section id="ext_menu-0">

    <nav class="navbar navbar-dropdown bg-color transparent navbar-fixed-top">
        <div class="container">

            <div class="sage-table">
                <div class="sage-table-cell">

                    <div class="navbar-brand">
                        <a href="#" class="navbar-logo"><img src="https://mobirise.com/assets3/images/logo.png" alt="Mobirise"></a>
                        <a class="navbar-caption" href="https://mobirise.com">MOBIRISE</a>
                    </div>

                </div>
                <div class="sage-table-cell">

                    <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        <div class="hamburger-icon"></div>
                    </button>
			
                    <ul class="nav-dropdown collapse pull-xs-right nav navbar-nav navbar-toggleable-sm" id="exCollapsingNavbar"><li class="nav-item"><a class="nav-link link" href="index.html#features6-3">WHAT IS IT</a></li><li class="nav-item"><a class="nav-link link" href="index.html#features6-9">FEATURES</a></li><li class="nav-item"><a class="nav-link link" href="http://forums.mobirise.com/">FORUMS</a></li><li class="nav-item nav-btn"><a class="nav-link btn btn-white btn-white-outline" href="index.html#msg-box3-19">DOWNLOAD</a></li></ul>
                    <button hidden="" class="navbar-toggler navbar-close" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        <div class="close-icon"></div>
                    </button>

                </div>
            </div>

        </div>
    </nav>

</section>>
